# Docker for Symfony2

## Install docker

> docker-compose up -d

## Create new project

> docker-compose run --rm symfony symfony new myproject

Then move your project to its parent directory

> mv ../project/* ..; mv ../project/.gitignore ..

## open a symfony shell

> docker-compose run --rm symfony app/console --shell

## Set database host in parameters.yml :

>    database_host: "%database.host%"
